using System;

namespace Heap
{
    public class Heap
    {
        private int[] _array;
        private int _heapSize;
        private int _d;

        public Heap(int[] array, int heapSize, int d)
        {
            _array = array;
            _heapSize = heapSize;
            _d = d;
        }

        public void Print()
        {
            for(int i=0;i<_heapSize-1;i++)
            {
                System.Console.Write($"{_array[i]}, ");
            }
            System.Console.Write($"{_array[_heapSize-1]}");
            System.Console.WriteLine();
        }

        public void PrintHeap()
        {
            var heapHeight = (int)Math.Floor(Math.Log(_heapSize*(_d-1)+2-_d, _d));
            System.Console.WriteLine($"Heap Height is: {heapHeight}");
            for (int i=0; i<=heapHeight;i++)
            {
                var numOfPossibleNodes = Math.Pow(_d, i);
                for (int j=0; j<numOfPossibleNodes;j++)
                {
                    int currentNodeIndex = j + (int)Math.Floor(Math.Pow(_d, i)/(_d-1));
                    if (currentNodeIndex<_heapSize)
                        System.Console.Write($"{_array[currentNodeIndex]} ");
                    else
                        break;
                }
                System.Console.WriteLine();
            }
        }

        public void BuildMaxHeap()
        {            
            for (int i=(int)Math.Floor((double)(_heapSize-2)/_d); i>=0;i--)
            {
                MaxHeapify(i);
            }
        }

        public void MaxHeapify(int index)
        {
            if (index >= _heapSize)
                throw new Exception("index is not in the heap");

            int largest = index;
            int largestValue = _array[index];
            var children = GetChildren(index);

            foreach (var child in children)
            {
                if(child < _heapSize && _array[child] > largestValue)
                {
                    largest = child;
                    largestValue = _array[largest];
                }
            }

            if (largest != index)
            {
                Swap(index, largest);
                MaxHeapify(largest);
            }
        }

        private int[] GetChildren(int index)
        {
            int[] children = new int[_d];
            for(int i=0; i<_d; i++)
                children[i] = Child(index, i + 1);
            
            return children;
        }

        private int Child(int index, int kChild)
        {
            return _d*index + kChild;
        }

        private int Parent(int index)
        {
            return (int)Math.Floor((double)(index-1)/_d);
        }

        private void Swap(int i, int j)
        {
            var temp = _array[i];
            _array[i] = _array[j];
            _array[j] = temp;
        }
    }
}