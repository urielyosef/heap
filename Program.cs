﻿using System;
using System.IO;
using System.Linq;

namespace Heap
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter d value: ");
            int d = int.Parse(Console.ReadLine());

            Console.Write("Please enter data file location: ");
            var path = Console.ReadLine();

            var data = ReadFromFile(path);
            var array = ParseDataToArray(data);

            // Console.WriteLine("Please enter heap size: ");
            // int heapSize = int.Parse(Console.ReadLine());
            // if(heapSize>array.Length)
            // {
            //     Console.WriteLine("Heap Size can't be bigger than array length");
            //     return;
            // }
            
            var heap = new Heap(array, array.Length, d);
            heap.BuildMaxHeap();
            heap.Print();
            heap.PrintHeap();
        }

        static string ReadFromFile(string path)
        {
            return File.ReadAllText(path);
        }

        static int[] ParseDataToArray(string data)
        {
            var stringArray = data.Split(',').ToArray();
            return Array.ConvertAll(stringArray, int.Parse);
        }
    }
}